#!/bin/bash
##############################################################################################
#  Copyright Accenture. All Rights Reserved.
#
#  SPDX-License-Identifier: Apache-2.0
##############################################################################################

set -e

echo "Starting build process..."

echo "Adding env variables..."
export PATH=/root/bin:$PATH

#Path to k8s config file
KUBECONFIG=/home/bevel/build/config


echo "Running the playbook..."
exec ansible-playbook -vv /home/osboxes/Documents/herbalcoin/platforms/shared/configuration/site.yaml --inventory-file=/home/osboxes/Documents/herbalcoin/platforms/shared/inventory/ -e "@/home/osboxes/Documents/herbalcoin/build/network.yaml" -e 'ansible_python_interpreter=/usr/bin/python3' -e "reset='true'"
